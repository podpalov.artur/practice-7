package DecimalCounter;

public class DecimalCounter {
    int counterValue;
    int minValue;
    int maxValue;

    public DecimalCounter(int minValue, int maxValue, int counterValue) {
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.counterValue = counterValue;
    }

    public DecimalCounter() {
        minValue = 0;
        maxValue = 15;
        counterValue = 14;
    }

    public void increase() {
        if (counterValue < maxValue) {
            counterValue++;
        }
    }

    public void decrease() {
        if (counterValue > minValue) {
            counterValue--;
        }
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public void setCounterValue(int counterValue) {
        this.counterValue = counterValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    public int getMinValue() {
        return minValue;
    }

    public int getCounterValue() {
        return counterValue;
    }
}

