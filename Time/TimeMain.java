package Time;

public class TimeMain {
    public static void main(String[] args) {
        Time clock = new Time(23, 33, 1);
        clock.addMinute(35);
        printTime(clock);
    }

    public static void printTime(Time clock) {
        int hour = clock.getHour();
        int min = clock.getMin();
        int sec = clock.getSec();
        String h;
        String m;
        String s;
        if (hour < 9) {
            h = "0" + Integer.toString(hour);
        } else {
            h = Integer.toString(hour);
        }
        if (min < 9) {
            m = "0" + Integer.toString(min);
        } else {
            m = Integer.toString(min);
        }
        if (sec < 9) {
            s = "0" + Integer.toString(sec);
        } else {
            s = Integer.toString(sec);
        }
        System.out.println(h + ":" + m + ":" + s);
    }
}
