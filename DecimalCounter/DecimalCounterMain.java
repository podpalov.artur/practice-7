package DecimalCounter;

public class DecimalCounterMain {
    public static void main(String[] args) {
        DecimalCounter counter = new DecimalCounter(1, 5, 4);
        counter.increase();
        System.out.println(counter.getCounterValue());
        counter.increase();
        System.out.println(counter.getCounterValue());

        counter.setCounterValue(3);
        counter.decrease();
        System.out.println(counter.getCounterValue());
        counter.decrease();
        System.out.println(counter.getCounterValue());
    }
}

