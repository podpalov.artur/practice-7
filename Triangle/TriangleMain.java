package Triangle;

public class TriangleMain {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(5, 8, 9);
        double area = triangle.getArea();
        double perimeter = triangle.getPerimeter();
        System.out.println("Area = " + area + " Perimeter = " + perimeter);
    }
}
