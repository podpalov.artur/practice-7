package Time;

public class Time {
    int hour;
    int min;
    int sec;

    public Time(int hour, int min, int sec) {
        setHour(hour);
        setMin(min);
        setSec(sec);
    }

    public void setHour(int hour) {
        if (hour >= 0 && hour < 24) {
            this.hour = hour;
        } else {
            this.hour = 0;
        }
    }

    public int getSec() {
        return sec;
    }

    public int getMin() {
        return min;
    }

    public int getHour() {
        return hour;
    }

    public void setMin(int min) {
        if (min >= 0 && min < 60) {
            this.min = min;
        } else {
            this.min = 0;
        }
    }

    public void setSec(int sec) {
        if (sec >= 0 && sec < 60) {
            this.sec = sec;
        } else {
            this.sec = 0;
        }
    }

    public void addHour(int inputHour) {
        int totalHour = hour + inputHour;
        hour = totalHour % 24;
    }

    public void addMinute(int inputMin) {
        int totalMin = min + inputMin;
        min = totalMin % 60;
        addHour(totalMin / 60);
    }

    public void addSec(int inputSec) {
        int totalSec = sec + inputSec;
        sec = totalSec % 60;
        addMinute(totalSec / 60);
    }
}


